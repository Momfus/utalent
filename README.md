﻿
# UTalent #
## ¿De qué trata? ##
 Sistema gamificado de seguimiento de la situación académica del estudiante universitario de forma visualmente clara e interactiva.
Motivo del proyecto
La vida del estudiante universitario es difícil, más cuando uno no encuentra una forma bien ordenada de qué materias puede cursar, cuales son correlativas con otras, en que situación de la carrera se encuentra y cuál es la especialidad que está siguiendo o le interesa seguir.

Lo anterior podría realizarse de manera tediosa y que el mismo estudiante lleve anotado que es lo que quiere estudiar en su carrera, aunque lo más seguro que al hacerlo le dará poco entusiasmo y no sentirá una forma de medir su logro visualmente.

La propuesta con esta aplicación es aplicar a lo descripto más arriba un proceso de “gamificación” para generar una sensación de recompensa al estudiante y que pueda seguir su estado, incluso fomentarlo en mejorar sus estudios y motivación personal. Se mostrará las carreras y materias como si fuera un árbol de habilidades (muy utilizado en videojuegos famosos) con información de la misma, recompensando con personalización de un avatar y tener ordenado el estado de la carrera del estudiante.
## Concepto de Gamificación ##

La gamificación consiste en aplicar mecánicas de juegos en ambientes naturalmente no lúdicos con el fin de potenciar la motivación, concentración, esfuerzo dedicado y muchos valores positivos que son asociados comúnmente a los juegos ya que el ser humano es un ser lúdico por naturaleza. Se está utilizando este concepto internacionalmente en centros educativos, preparación de personal, como medio para influir y motivar a grupos de personas en empresas e incluso en la medicina (Khan Academy, Treehouse, Dungeon And Developers, Chore Wars, etcétera).

Los videojuegos han tomado el papel del medio de entretenimiento masivo más importante del siglo XXI y muchos de los conceptos utilizados para aprender cómo se juegan y motivar a la persona a que siga jugando prestando atención al contenido es casi su esencia.


### Tablero de Trello ###
Link: https://trello.com/b/4DRnQRwb


### Lenguaje a utilizar ###
En el 2017: se está usando el framwork Angular (posterior al 1 debido a que hicieron varios cambios al 2 y luego se mantuvo en siguientes versiones)

En el 2016: se usó para prototipo Godot Engine
